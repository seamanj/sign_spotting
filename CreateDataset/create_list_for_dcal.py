import os
import argparse
import pickle
import random
from typing import NamedTuple, List
from collections import Counter
from word_tokenizer import my_word_tokenizer
from dataset_utils import create_dataset, print_dataset, write_dataset
from fixes import typos, unknowns, dashes

def main(params):

    input_dir = params.input_dir
    output_dir = params.output_dir

    ann_files = [os.path.join(p, y) for p, _, x in os.walk(input_dir) for y in x]

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    #
    # annotations = []
    # for af in ann_files:
    #     with open(af, "rb") as f:
    #         annotations.append(pickle.load(f))

    free_translations = []
    file_names = []
    for af in ann_files:
        with open(af, "rb") as f:
            ann = pickle.load(f)
            for ft in ann.FreeTrans:
                if ft.annotation.annotation_value:
                    file_names.append(ann.FileName)
                    free_translations.append(ft.annotation.annotation_value)

    # Parantheses
    p_file = os.path.join(output_dir, 'parantheses.txt')
    with open(p_file, 'w') as f:
        for a, s in zip(file_names, free_translations):
            if "(" in s:
                f.write("File Name: {}\n".format(a))
                f.write("Annotation: {}\n\n".format(s))

    # Typos
    t_file = os.path.join(output_dir, 'typos.txt')
    with open(t_file, 'w') as f:
        for t in typos:
            for a, s in zip(file_names, free_translations):
                if t[0] in s:
                    f.write("File Name: {}\n".format(a))
                    f.write("Typo: {} => {}\n".format(t[0], t[1]))
                    f.write("Original Annotation: {}\n".format(s))
                    f.write("Fixed Annotation: {}\n\n".format(s.replace(t[0],t[1])))

    # Unknown
    u_file = os.path.join(output_dir, 'unknown.txt')
    with open(u_file, 'w') as f:
        for t in unknowns:
            for a, s in zip(file_names, free_translations):
                if t[0] in s:
                    f.write("File Name: {}\n".format(a))
                    f.write("Unknown: {} => {}\n".format(t[0], t[1]))
                    f.write("Original Annotation: {}\n".format(s))
                    f.write("Fixed Annotation: {}\n\n".format(
                        s.replace(t[0], t[1])))

    # Dashes
    d_file = os.path.join(output_dir, 'dashes.txt')
    with open(d_file, 'w') as f:
        for t in dashes:
            for a, s in zip(file_names, free_translations):
                if t[0] in s:
                    f.write("File Name: {}\n".format(a))
                    f.write("Dash Typo: {} => {}\n".format(t[0], t[1]))
                    f.write("Original Annotation: {}\n".format(s))
                    f.write("Fixed Annotation: {}\n\n".format(
                        s.replace(t[0], t[1])))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, default=None, help="")
    parser.add_argument("--output_dir", type=str, default="", help="")
    params, _ = parser.parse_known_args()
    main(params)
