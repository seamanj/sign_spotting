import xmltodict
import argparse
import os
import pickle
import gzip
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import cv2

def make_dir(dir: str) -> str:
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def save_gklz(obj, filename):
    with gzip.open(filename, "wb") as f:
        pickle.dump(obj, f, -1)



def main(params):

    annotation_file = params.annotation_file
    # annotation_root = params.annotation_root
    video_root = params.video_root
    output_root = params.output_root
    # file_name = annotation_file.split("/")[-1]
    # sub_dir = annotation_file.replace(input_root, '')
    # sub_dir = sub_dir.replace(file_name,'')


    print('open annotation file:{}'.format(annotation_file))
    with open(annotation_file) as fd:
        doc = xmltodict.parse(fd.read())["ANNOTATION_DOCUMENT"]

    right_valid = False
    left_valid = False
    for tier in doc["TIER"]:
        if tier["@TIER_ID"] == "RH-IDgloss" and "ANNOTATION" in tier:
            right_valid = True
            RH_IDgloss = tier
        if tier["@TIER_ID"] == "LH-IDgloss" and "ANNOTATION" in tier:
            left_valid = True
            LH_IDgloss = tier
    if not (right_valid or left_valid):
        print('early returned because no gloss info found')
        return


    valid = False
    header = doc["HEADER"]
    num_MEDIA_DESCRIPTOR = len(header["MEDIA_DESCRIPTOR"])

    if type(header["MEDIA_DESCRIPTOR"]) is not list:# tj : deal with only one MEDIA_DESCRIPTOR element
        media = header["MEDIA_DESCRIPTOR"]
        video_id = (
            media["@MEDIA_URL"]
                .split("/")[-1]
                .split(".")[0]
                .replace("-comp", "")
                .replace("-c", "")
        )
        if "-" not in video_id and (("(" in video_id) or ("+" not in video_id)):
            valid = True
            video_name = video_id
            offset = float(media.get("@TIME_ORIGIN", 0))
    else:
        for media in header["MEDIA_DESCRIPTOR"]:
            video_id = (
                media["@MEDIA_URL"]
                    .split("/")[-1]
                    .split(".")[0]
                    .replace("-comp", "")
                    .replace("-c", "")
            )
            if "-" not in video_id and (("(" in video_id) or ("+" not in video_id)):
                valid = True
                video_name = video_id
                offset = float(media.get("@TIME_ORIGIN", 0))

    if not valid:
        return

    # tj : find the video file


    video_files = [
        {"file_path": os.path.join(root, name)}
        for root, _, files in os.walk(video_root)
        for name in files
        if name == '{}.mov'.format(video_name)
    ]

    print('{} video_file matched {}.mov found'.format(len(video_files), video_name))

    if len(video_files) != 1:
        print('searching video_files error')
        return

    video_path = video_files[0]["file_path"]
    # tj : count frame number
    vidcap = cv2.VideoCapture(video_path)
    num_video_frames = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    if num_video_frames <= 0:
        print('video has zero frame')
        return

    relative_dir = os.path.relpath(video_path, video_root)
    sub_dir = os.path.join(*relative_dir.split('/')[:-1])

    signing = np.zeros((num_video_frames), dtype=np.bool)

    gloss = ["" for i in range(num_video_frames)]
    # gloss[0] = 'hello'

    time_order = doc["TIME_ORDER"]["TIME_SLOT"]
    time_slots = {
        ts["@TIME_SLOT_ID"]: float(ts["@TIME_VALUE"])
        for ts in time_order
    }

    if right_valid:
        for ann in RH_IDgloss["ANNOTATION"]:
            start_ms = (
                time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]]
                + offset
            )
            stop_ms = (
                time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]]
                + offset
            )
            translation = ann["ALIGNABLE_ANNOTATION"]["ANNOTATION_VALUE"]

            start_frame = math.ceil(25.0 * (start_ms / 1000.0)) + 2
            stop_frame = math.floor(25.0 * (stop_ms / 1000.0)) + 1
            signing[start_frame:stop_frame] = 1
            for i in range(start_frame, stop_frame):
                gloss[i] = translation


    if left_valid:
        for ann in LH_IDgloss["ANNOTATION"]:
            start_ms = (
                time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]]
                + offset
            )
            stop_ms = (
                time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]]
                + offset
            )
            translation = ann["ALIGNABLE_ANNOTATION"]["ANNOTATION_VALUE"]

            start_frame = math.ceil(25.0 * (start_ms / 1000.0)) + 2
            stop_frame = math.floor(25.0 * (stop_ms / 1000.0)) + 1
            signing[start_frame:stop_frame] = 1
            for i in range(start_frame, stop_frame):
                gloss[i] = translation


    save_data = {}
    save_data['video_path'] = video_path
    save_data['signing'] = signing
    save_data['gloss'] = gloss

    output_dir = os.path.join(output_root, sub_dir)
    make_dir(output_dir)

    output_file = os.path.join(output_dir, "{}.gklz".format(video_name))
    save_gklz(obj=save_data, filename=output_file)
    print('{} saved'.format(output_file))

def load_data():
    a = load_gklz("/home/seamanj/Workspace/BSLCP/gloss/Conversation/London/1+2/L1c.gklz")
    # a = load_gklz("/vol/research/extol/tmp/Tao_BSLCP/gloss/Conversation/London/32+33/L33c.gklz")
    print('1')

if __name__ == "__main__":

    load_data()

    # Assumes they are in the same order
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--annotation_file",
        type=str,
        default="/home/seamanj/Workspace/BSLCP/Annotations.DCAL/Conversation/London/LN01M57WHC_LH.eaf",
    )

    # parser.add_argument(
    #     "--annotation_root",
    #     type=str,
    #     default="/vol/research/extol/data/BSLCP/Annotations.DCAL/",
    #     help="",
    # )
    parser.add_argument(
        "--video_root",
        type=str,
        # default="/vol/research/extol/tmp/Tao_BSLCP/videos-resized-25fps-256x256",
        default="/home/seamanj/Workspace/BSLCP/videos-resized-25fps-256x256",
    )
    parser.add_argument(
        "--output_root",
        type=str,
        # default='/vol/research/extol/tmp/Tao_BSLCP/gloss',
        default="/home/seamanj/Workspace/BSLCP/gloss",
    )
    params, _ = parser.parse_known_args()

    main(params)
