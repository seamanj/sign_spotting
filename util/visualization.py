import xmltodict
import argparse
import os
import pickle
import gzip
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import cv2
from tqdm import tqdm
from utils.vizutils import *
def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def main():
    # gloss_file = "/vol/research/extol/tmp/Tao_BSLCP/wordspotting/Conversation/Birmingham/13+14/BM14c.gklz"
    data = load_gklz("/home/seamanj/Workspace/BSLCP/wordspotting/Conversation/London/32+33/L33c.gklz")

    video_path = data['video_path']

    # video_path = "/home/seamanj/Workspace/BSLCP/test.mp4"
    save_path = os.path.join(*video_path.split('.')[:-1])
    signing = data['signing']
    gloss = data['gloss']


    suffix = ".avi"
    fourcc = cv2.VideoWriter_fourcc("M", "J", "P", "G")


    out = cv2.VideoWriter(str(save_path) + suffix, fourcc, 2, (1000, 1000))
    vidcap = cv2.VideoCapture(video_path) # tj : load video
    # nframes = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    nframes = len(data['gloss'])


    win, fig = None, None
    for t in tqdm(range(4625,nframes)):
        vidcap.set(cv2.CAP_PROP_POS_FRAMES, t)
        success, bgr = vidcap.read()
        if success:
            frame_color = 'blue'
            if signing[t]:
                frame_color = 'green'

            img = text_on_image(bgr, txt="frame: {}, {}".format(t, 'none' if gloss[t] == '' else gloss[t]))
            img = rectangle_on_image(img, frame_color=frame_color)

            if not win:
                fig = plt.figure(figsize=(20,20))
                win = plt.imshow(img)
            else:
                win.set_data(img)

            fig_img = fig2data(fig)
            # fig_img = scipy.misc.imresize(fig_img, [1000, 1000])
            fig_img = np.array(Image.fromarray(fig_img).resize([1000, 1000]))
            out.write(fig_img)
    out.release()


    print('hello')




if __name__ == "__main__":
    main()