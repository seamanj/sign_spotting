from torch.utils.data import Dataset, DataLoader



class MyDataset(Dataset):
    def __init__(self):
        self.samples = []


    def __len__(self):
        """
        Return the total number of samples
        """
        return len(self.samples)  # Hard Code

    def __getitem__(self, index):
        """
        Generate one sample of the dataset
        """
        sample = {}
        sample['input'] = self.samples[index]
        return sample



if __name__ == "__main__":
    train_dataset = MyDataset()
    val_dataset = MyDataset()

    for i in range(100):
        if i % 4 == 0:
            val_dataset.samples.append(i)
        else:
            train_dataset.samples.append(i)

    print(train_dataset.__len__())
    print(val_dataset.__len__())

    print(train_dataset.__getitem__(0))
