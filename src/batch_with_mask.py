import torch
from torch import tensor

class BatchWithMask:
    def __init__(self, torch_batch, mask, use_cuda=False):
        self.skeleton = torch_batch
        self.mask = mask
        self.use_cuda = use_cuda

        if use_cuda:
            self._make_cuda()

    def _make_cuda(self):
        self.skeleton = self.skeleton.cuda()
        self.mask = self.mask.cuda()



class BatchWithMaskValid:
    def __init__(self, torch_batch, mask, valid=None, use_cuda=False):
        self.skeleton = torch_batch
        self.mask = mask
        self.valid = valid
        self.use_cuda = use_cuda

        if use_cuda:
            self._make_cuda()

    def _make_cuda(self):
        self.skeleton = self.skeleton.cuda()
        self.mask = self.mask.cuda()
        if self.valid is not None:
            self.valid = self.valid.cuda()



class BatchWithMaskCls:
    def __init__(self, torch_batch, mask, cls=None, use_cuda=False):
        self.skeleton = torch_batch
        self.mask = mask.unsqueeze(1)
        self.cls = cls
        self.use_cuda = use_cuda

        if use_cuda:
            self._make_cuda()

    def _make_cuda(self):
        self.skeleton = self.skeleton.cuda()
        self.mask = self.mask.cuda()
        if self.cls is not None:
            self.cls = self.cls.cuda()

class BatchWithMaskValidCls:
    def __init__(self, torch_batch, feature_mask, src_mask, seg, valid=None, cls=None, use_cuda=False):
        self.skeleton = torch_batch
        self.feature_mask = feature_mask
        self.src_mask = src_mask.unsqueeze(1) # tj : need unsqueeze 1
        self.seg = seg
        self.valid = valid
        self.cls = cls
        self.use_cuda = use_cuda

        if use_cuda:
            self._make_cuda()

    def _make_cuda(self):
        self.skeleton = self.skeleton.cuda()
        self.feature_mask = self.feature_mask.cuda()
        self.src_mask = self.src_mask.cuda()
        self.seg = self.seg.cuda()
        if self.valid is not None:
            self.valid = self.valid.cuda()
        if self.cls is not None:
            self.cls = self.cls.cuda()


class MyBatch:
    def __init__(self, input, cls, use_cuda=False):
        self.input = input
        self.cls = cls
        self.use_cuda = use_cuda

        if use_cuda:
            self._make_cuda()

    def _make_cuda(self):
        self.input = self.input.cuda()
        self.cls = self.cls.cuda()

