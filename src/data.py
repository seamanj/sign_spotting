import torch
from torch.utils.data import Dataset, DataLoader
import os
from glob import glob
import json
from src.helpers import *
import cv2
from utils import *
import pickle
from torch.utils.data.dataloader import default_collate
from src.spatial_transforms import *
from src.my_collate import *
from typing import Dict, List, Tuple, Union  # tj : to process None in __getitem__ in dataset
# https://discuss.pytorch.org/t/questions-about-dataloader-and-dataset/806/4
# def my_collate(batch):
#     "Puts each data field into a tensor with outer dimension batch size"
#     batch = list(filter(lambda x : x is not None, batch))
#     return default_collate(batch)

def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object

def make_data_loader(dataset: Dataset,
                     batch_size: int,
                     shuffle: bool = False) -> DataLoader:
    params = {
        'batch_size': batch_size,
        'shuffle': shuffle,
        'num_workers': 4,
        'collate_fn': my_collate}
    return DataLoader(dataset, **params)



def load_train_data(data_cfg: dict, spatial_transform, temporal_transform, spatiotemporal_transform) -> (Dataset):  # tj : for czech data
    # = tj : print the train path

    data_path = data_cfg.get("train_data_path")

    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    sample_stride = data_cfg.get("sample_stride")

    local_path = data_cfg.get("local_path")
    window_success_threshold = data_cfg.get("window_success_threshold")
    # window_translated_threshold = data_cfg.get("window_translated_threshold")
    return MyDataset(data_path, 'train', window_size, window_stride, sample_stride, local_path)

def load_dev_data(data_cfg: dict, spatial_transform, temporal_transform, spatiotemporal_transform) -> (Dataset, Dataset):  # tj : for czech data
    # = tj : print the train path

    data_path = data_cfg.get("dev_data_path")

    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    sample_stride = data_cfg.get("sample_stride")
    local_path = data_cfg.get("local_path")

    window_success_threshold = data_cfg.get("window_success_threshold")

    return MyDataset(data_path, 'val', window_size, window_stride, sample_stride, local_path)


def load_train_val_data(data_cfg: dict) -> (Dataset, Dataset):

    data_path = data_cfg.get('train_val_data_path')
    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")

    train_dataset = MyDataset("train", window_size)
    val_dataset = MyDataset('val', window_size)

    seqs = [
        {
            "full_path": os.path.join(root, y),
            "file_name": y[:y.rfind(".gklz")]
        }
        for root, _, files in os.walk(data_path)
        for y in files
        if y.split(".")[-1] == "gklz"
    ]

    for seq in seqs:
        print("start loading %s" % seq["full_path"])
        data = load_gklz(seq["full_path"])

        video_path = data['video_path']
        signing = data['signing']
        gloss = data['gloss']
        i3d_feature = data['i3d_feature']

        num_video_frames = len(signing)

        i = 0
        num_added = 0
        while True:
            if i + window_size > num_video_frames:
                break

            diff = signing[i+1:i+window_size] ^ signing[i:i + window_size - 1]

            if( sum(diff) >= 6 ):
                if num_added % 4 == 0:
                    val_dataset.samples.append({'video_path': video_path, 'signing': signing[i:i + window_size],
                                                'gloss': gloss[i:i + window_size],
                                                'start_frame': i,
                                                'i3d_feature': i3d_feature[i:i+window_size]})
                    i += window_size - window_stride # tj : we  don't want overlap
                else:
                    train_dataset.samples.append({'video_path': video_path, 'signing': signing[i:i+window_size],
                                                  'gloss': gloss[i:i+window_size],
                                                  'start_frame': i,
                                                  'i3d_feature': i3d_feature[i:i+window_size]})
                    if num_added % 4 == 3: # tj : the last train data, for valid data we don't want overlap
                        i += window_size - window_stride

                num_added += 1

            i += window_stride
    print("")
    return train_dataset, val_dataset


def load_test_data(data_cfg: dict) -> (Dataset):  # tj : for czech data
    # = tj : print the train path
    test_path = data_cfg.get("test_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")

    window_success_threshold = data_cfg.get("window_success_threshold")

    return MyDataset(test_path, window_size, window_stride)

def load_eval_data(data_cfg: dict) -> (Dataset):  # tj : for czech data
    # = tj : print the train path
    eval_path = data_cfg.get("eval_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    # max_sentence_length = data_cfg.get("max_sentence_length")
    # min_sentence_length = data_cfg.get("min_sentence_length")
    # down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    # window_translated_threshold = data_cfg.get("window_translated_threshold")
    return MyDataset(eval_path,  window_size, window_stride)



class MyDataset(Dataset):
    def __init__(self, set_name, window_size):
        self.samples = []
        self.set_name = set_name
        self.window_size = window_size

    def gpu_collater(self, minibatch, concat_datasets=None):
        rgb = minibatch["rgb"]
        assert rgb.is_cuda, "expected tensor to be on the GPU"
        if self.set_name == "train":
            is_hflip = random.random() < 0.5
            if is_hflip:
                # horizontal axis is last
                rgb = torch.flip(rgb, dims=[-1])

        if self.set_name == "train":
            rgb = im_color_jitter(rgb, num_in_frames=self.window_size, thr=0.2)

        # For now, mimic the original pipeline.  If it's still a bottleneck, we should
        # collapse the cropping, resizing etc. logic into a single sampling grid.
        iB, iC, iK, iH, iW = rgb.shape
        assert iK == self.window_size, "unexpected number of frames per clip"


        self.scale_factor = 0.1
        self.inp_res = 224
        self.resize_res = 256
        self.mean = 0.5 * torch.ones(3)
        self.std = 1.0 * torch.ones(3)

        grids = torch.zeros(
            iB, self.inp_res, self.inp_res, 2, device=rgb.device, dtype=rgb.dtype
        )

        yticks = torch.linspace(start=-1, end=1, steps=self.inp_res)
        xticks = torch.linspace(start=-1, end=1, steps=self.inp_res)
        grid_y, grid_x = torch.meshgrid(yticks, xticks)
        # The grid expects the ordering to be x then y
        for ii in range(iB):
            grids[ii] = torch.stack((grid_x, grid_y), 2)


        # merge RGB and clip dimensions to use with grid sampler
        rgb = rgb.view(rgb.shape[0], 3 * self.window_size, iH, iW)
        rgb = torch.nn.functional.grid_sample(
            rgb, grid=grids, mode="bilinear", align_corners=False, padding_mode="zeros",
        )
        # unflatten channel/clip dimension
        rgb = rgb.view(rgb.shape[0], 3, self.window_size, self.inp_res, self.inp_res)
        rgb = color_normalize(rgb, self.mean, self.std)
        minibatch["rgb"] = rgb
        return minibatch

    def __len__(self):
        """
        Return the total number of samples
        """
        return len(self.samples)  # Hard Code

    def __getitem__(self, index):
        """
        Generate one sample of the dataset
        """
        sample = {}
        seq = self.samples[index]
        vidcap = cv2.VideoCapture(seq["video_path"])
        t = seq['start_frame']
        frame_indices = range(t, t + self.window_size)

        rgbs = []
        for i in frame_indices:
            vidcap.set(cv2.CAP_PROP_POS_FRAMES, i)
            success, bgr = vidcap.read()
            # BGR (OpenCV) to RGB (Torch)
            if success:
                rgb = bgr[:, :, [2, 1, 0]]
                chw = np.transpose(rgb, (2, 0, 1))  # C*H*W
                rgbs.append(chw)
            else:
                return None

        tchw = np.stack(rgbs, axis=0)
        cthw = np.transpose(tchw, (1, 0, 2, 3))
        # cthw = tchw.permute(1, 0, 2, 3)

        sample['rgb'] = (cthw / 255).astype(np.float32)  # tj : TCHW -> CTHW


        sample['input'] = seq['i3d_feature']

        sample['cls'] = 1 - seq['signing'] # tj : boundary
        sample['gloss'] = seq['gloss']
        return sample

