import torch
from src.skeletalModel import getSkeletalModelStructure
import numpy as np

def make_noise(src, mask, noise_weight=0.):
    if len(mask.shape) == 2:
        limb = getSkeletalModelStructure()
        limb = np.asarray(limb)
        end_index = np.concatenate(([0],limb[:,1]))
        start_index = np.concatenate(([0],limb[:,0]))
        idx = np.argsort(end_index)
        end_index = end_index[idx]
        start_index = start_index[idx]
        limb_len = src[:,:,end_index,:] - src[:,:,start_index,:]
        limb_len = torch.norm(limb_len, p=2, dim=-1)
        limb_len = torch.mean(limb_len, dim=1, keepdim=True)
        limb_len = limb_len.repeat(src.shape[3], 1, src.shape[1], 1).permute(1,2,3,0)
        limb_len_np = limb_len.detach().cpu().numpy()
        noise = np.random.uniform(-noise_weight*limb_len_np, noise_weight*limb_len_np, src.shape).astype(np.float32)
        noise = torch.from_numpy(noise).cuda()
        src[mask,:,:] = src[mask,:,:] + noise[mask,:,:]