import getpass
from os.path import dirname

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
import torch
from PIL import Image

from utils.imutils import rectangle_on_image, text_on_image

# from .misc import mkdir_p, to_numpy
import errno
import os
def mkdir_p(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def to_numpy(tensor):
    if torch.is_tensor(tensor):
        return tensor.cpu().numpy()
    elif type(tensor).__module__ != "numpy":
        raise ValueError(f"Cannot convert {type(tensor)} to numpy array")
    return tensor


def im_to_numpy(img):
    img = to_numpy(img)
    img = np.transpose(img, (1, 2, 0))  # H*W*C
    return img

def is_show(num_figs, iter_no, epoch_len):
    if num_figs == 0:
        return 0
    show_freq = int(epoch_len / num_figs)
    if show_freq != 0:
        return iter_no % show_freq == 0
    else:
        return 1


def _imshow_pytorch(rgb, ax=None):
    # from utils.transforms import im_to_numpy

    if not ax:
        fig = plt.figure()
        ax = fig.add_subplot(111)
    ax.imshow(im_to_numpy(rgb * 255).astype(np.uint8))
    ax.axis("off")


def fig2data(fig):
    """
    @brief Convert a Matplotlib figure to a 3D numpy array with RGB channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGB values
    """
    # draw the renderer
    fig.canvas.draw()
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep="")
    buf = buf.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    return buf


def viz_sample(img, out_torch, gloss, targets, t):
    # Note: use class_names from the first batch because they are duplicates.
    # It might become an issue when several datasets are concatenated,
    # But usually same vocab are concatenated
    if isinstance(out_torch, torch.FloatTensor):  # Prediction
        outs = torch.nn.functional.softmax(out_torch, dim=1).data
        # v, out = torch.max(out, 0)
        # out = out.item()
        # frame_color = "green" if target == out else "red"
        img = text_on_image(img, txt='None' if gloss == None else gloss, gt=targets, pre=outs[:,1], t=t)
        # img = rectangle_on_image(img, frame_color=frame_color)
    else:  # Ground truth
        out = out_torch
        img = text_on_image(img, txt='None' if gloss == None else gloss)
    return img


def viz_batch(
    inputs,
    outputs,
    targets,
    t,
    gloss,
    mean=torch.Tensor([0.5, 0.5, 0.5]),
    std=torch.Tensor([1.0, 1.0, 1.0])
):
    batch_img = []
    for n in range(min(inputs.size(0), 1)):
        inp = inputs[n]
        # Un-normalize
        inp = (inp * std.view(3, 1, 1).expand_as(inp)) + mean.view(3, 1, 1).expand_as(
            inp
        )
        # Torch to numpy
        inp = to_numpy(inp.clamp(0, 1) * 255)
        inp = inp.transpose(1, 2, 0).astype(np.uint8)
        # Resize 256x256 to 512x512 to be bigger
        # inp = scipy.misc.imresize(inp, [256, 256])
        batch_img.append(
            viz_sample(
                inp, outputs[n], gloss=None if gloss == None else gloss[n], targets=targets[n], t=t
            )
        )
    return np.concatenate(batch_img)


def viz_gt_pred(
    inputs,
    outputs,
    targets,
    glosses,
    mean,
    std,
    win,
    fig,
    save_path=None,
    show=False,
):
    if save_path is not None:
        mkdir_p(dirname(save_path))
    # Save video viz
    if inputs[0].dim() == 4:
        # In case GPU preprocessing was used, we copy to the CPU
        data = inputs.cpu()
        suffix = ".avi"
        fourcc = cv2.VideoWriter_fourcc("M", "J", "P", "G")
        nframes = inputs.size(2)
        out = cv2.VideoWriter(str(save_path) + suffix, fourcc, 10, (1000, 1000))

        # For each frame
        for t in range(0, nframes, 1):
            inp = data[:, :, t, :, :]
            # This was used to save the original-res inputs for the supmat
            win, fig = viz_gt_pred_single(
                inp,
                outputs,
                targets,
                t,
                glosses[t],
                mean,
                std,
                win,
                fig,
                show,
            )
            fig_img = fig2data(fig)
            # fig_img = scipy.misc.imresize(fig_img, [1000, 1000])
            fig_img = np.array(Image.fromarray(fig_img).resize([1000, 1000]))
            out.write(fig_img[:, :, (2, 1, 0)])
        out.release()
    return win, fig


def viz_gt_pred_single(
    inputs, outputs, targets, t, gloss, mean, std,  win, fig,  show
):
    # print(inputs[:, 0, :5, 0])  # inputs: [10, 3, 256, 256]
    batch_img = viz_batch(
        inputs, outputs, targets, t, gloss=gloss, mean=mean, std=std )

    if not win:
        fig = plt.figure(figsize=(20, 20))
        win = plt.imshow(batch_img)

    else:
        win.set_data(batch_img)

    if show:
        print("Showing")
        plt.pause(0.05)

    return win, fig
