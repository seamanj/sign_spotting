import torch
from cnn_pretraining.helpers import load_config, set_seed, make_model_dir, make_logger,\
    symlink_update, load_checkpoint, ConfigurationError, log_cfg, append_file, freeze_params, f1_score_numpy
from cnn_pretraining.data import load_data, make_data_loader, load_train_data, load_val_data
from torch.utils import data
from cnn_pretraining.model import build_model
from cnn_pretraining.model import Model
from torch.utils.tensorboard import SummaryWriter
from cnn_pretraining.builders import build_gradient_clipper, build_optimizer, build_scheduler
import numpy as np
import os
from torch.utils.data import Dataset
import time
from cnn_pretraining.batch_with_mask import BatchWithMask, BatchWithMask, BatchWithMaskValid, BatchWithMaskValidCls, BatchWithMaskCls, MyBatch
from torch import Tensor
import queue
import shutil
from cnn_pretraining.mask import gen_mask, gen_translated_mask, gen_joint_mask_option, gen_valid_mask

from cnn_pretraining.center_loss import CenterLoss
from cnn_pretraining.prediction import validate

from cnn_pretraining.loss import MyLoss
from cnn_pretraining.draw import draw_ROC
import gc
from cnn_pretraining.helpers import freeze_params


class TrainManager:
    def __init__(self, model: Model, config: dict) -> None:

        train_config = config["training"]
        data_config = config["data"]


        # CPU / GPU, tj : this should be before build_optimizer, so that optimizer will know on what device to build
        self.use_cuda = train_config["use_cuda"]


        # self.loss.cuda()
        self.model = model
        if self.use_cuda:
            self.model.cuda()


        self.loss = MyLoss()


        #optimization
        self.learning_rate_min = train_config.get("learning_rate_min", 1.0e-8)
        self.clip_grad_fun = build_gradient_clipper(config=train_config)
        self.optimizer = build_optimizer(config=train_config,
                                         parameters=model.parameters())


        # validation & early stopping
        self.validation_freq = train_config.get("validation_freq", 1000)
        self.max_validation_batches = train_config.get("max_validation_batches", 0)
        self.ckpt_queue = queue.Queue(
            maxsize=train_config.get("keep_last_ckpts", 5))
        self.eval_metric = train_config.get("eval_metric", "bleu")

        self.early_stopping_metric = train_config.get("early_stopping_metric",
                                                      "loss")

        # if we schedule after BLEU/chrf, we want to maximize it, else minimize
        # early_stopping_metric decides on how to find the early stopping point:
        # ckpts are written when there's a new high/low score for this metric
        if self.early_stopping_metric in ["ppl", "loss"]:
            self.minimize_metric = True
        elif self.early_stopping_metric in ["VAL", "acc"]:
            self.minimize_metric = False
        else:
            raise ConfigurationError(
                "Invalid setting for 'early_stopping_metric', "
                "valid options: 'loss', 'ppl'.")

        # learning rate sheduling
        self.scheduler, self.scheduler_step_at = build_scheduler(
            config=train_config,
            scheduler_mode="min" if self.minimize_metric else "max",
            optimizer=self.optimizer,
            hidden_size=config["model"]["encoder"]["hidden_size"])

        # data & batch handling
        self.shuffle = train_config.get("shuffle", True)
        self.max_epoch = train_config["max_epoch"]
        # self.epoch_size = train_config['epoch_size']
        self.batch_size = train_config["batch_size"]

        # self.class_per_batch = train_config["class_per_batch"]
        # self.vidoes_per_class = train_config["vidoes_per_class"]

        # batch_size = class_per_batch * vidoes_per_class

        self.eval_batch_size = train_config["eval_batch_size"]
        self.batch_multiplier = train_config.get("batch_multiplier", 1)

        self.alpha = train_config['alpha']


        self.mask_frames = train_config["mask_frames"]
        self.eval_mask_frames = train_config["eval_mask_frames"]


        self.masking_times = train_config["masking_times"]



        # initialize training statistics
        self.steps = 0
        # stop training if this flag is True by reaching learning rate minimum
        self.stop = False

        self.epoch = 0

        self.best_ckpt_iteration = 0
        # initial values for best scores
        self.best_ckpt_score = np.inf if self.minimize_metric else -np.inf
        # comparison function for scores
        self.is_best = lambda score: score < self.best_ckpt_score \
            if self.minimize_metric else score > self.best_ckpt_score


        self.model_dir, init = make_model_dir(train_config["model_dir"],  overwrite=train_config.get("overwrite"))
        self.logger = make_logger("{}/train.log".format(self.model_dir))


        if init is True:  # tj : init from last exit
            self.init_from_checkpoint(os.path.join(self.model_dir, "best.ckpt"))
        else: # tj : check shall we start based on other pre-trained model
            # model parameters
            if "load_model" in train_config.keys():
                model_load_path = train_config["load_model"]
                self.logger.info("Loading model from %s", model_load_path)
                reset_epoch = train_config.get("reset_epoch", False)
                reset_steps = train_config.get("reset_steps", False)
                reset_best_ckpt = train_config.get("reset_best_ckpt", False)
                reset_scheduler = train_config.get("reset_scheduler", False)
                reset_optimizer = train_config.get("reset_optimizer", False)
                self.init_from_checkpoint(model_load_path,
                                          reset_epoch=reset_epoch,
                                          reset_steps=reset_steps,
                                          reset_best_ckpt=reset_best_ckpt,
                                          reset_scheduler=reset_scheduler,
                                          reset_optimizer=reset_optimizer)


        self.logging_freq = train_config.get("logging_freq", 100)
        self.valid_log_file = "{}/validations.log".format(self.model_dir)
        self.valid_report_file = "{}/validations.txt".format(self.model_dir)
        self.tb_writer = SummaryWriter(log_dir=self.model_dir + "/tensorboard/")

        self._log_parameters_list()


    def _save_checkpoint(self) -> None:
        model_path = "{}/{}.ckpt".format(self.model_dir, self.steps)
        state = {
            "epoch": self.epoch,
            "steps": self.steps,
            "best_ckpt_score": self.best_ckpt_score,
            "best_ckpt_iteration": self.best_ckpt_iteration,
            # "model_state": self.model.state_dict(),

            "cnn": self.model.cnn.state_dict(),
            "pose_regressor": self.model.pose_regressor.state_dict(),

            "optimizer_state": self.optimizer.state_dict(),
            "scheduler_state": self.scheduler.state_dict() if \
            self.scheduler is not None else None,
        }
        torch.save(state, model_path)
        if self.ckpt_queue.full():
            to_delete = self.ckpt_queue.get()  # delete oldest ckpt
            try:
                os.remove(to_delete)
            except FileNotFoundError:
                self.logger.warning("Wanted to delete old checkpoint %s but "
                                    "file does not exist.", to_delete)

        self.ckpt_queue.put(model_path)

        best_path = "{}/best.ckpt".format(self.model_dir)
        try:
            # create/modify symbolic link for best checkpoint
            symlink_update("{}.ckpt".format(self.steps), best_path)
        except OSError:
            # overwrite best.ckpt
            torch.save(state, best_path)



    def init_from_checkpoint(self, path: str,
                             reset_epoch: bool = False,
                             reset_steps: bool = False,
                             reset_best_ckpt: bool = False,
                             reset_scheduler: bool = False,
                             reset_optimizer: bool = False) -> None:

        model_checkpoint = load_checkpoint(path=path, use_cuda=self.use_cuda)

        # restore model and optimizer parameters
        self.model.cnn.load_state_dict(model_checkpoint["cnn"])
        self.model.pose_regressor.load_state_dict(model_checkpoint["pose_regressor"])

        if not reset_optimizer:
            self.optimizer.load_state_dict(model_checkpoint["optimizer_state"])
        else:
            self.logger.info("Reset optimizer.")

        if not reset_scheduler:
            if model_checkpoint["scheduler_state"] is not None and \
                    self.scheduler is not None:
                self.scheduler.load_state_dict(
                    model_checkpoint["scheduler_state"])
        else:
            self.logger.info("Reset scheduler.")

        # restore counts

        if not reset_epoch:
            self.epoch = model_checkpoint["epoch"] + 1
        else:
            self.logger.info("Reset epoch.")

        if not reset_steps:
            self.steps = model_checkpoint["steps"]
        else:
            self.logger.info("Reset steps.")

        if not reset_best_ckpt:
            self.best_ckpt_score = model_checkpoint["best_ckpt_score"]
            self.best_ckpt_iteration = model_checkpoint["best_ckpt_iteration"]
        else:
            self.logger.info("Reset tracking of the best checkpoint.")

        # move parameters to cuda
        if self.use_cuda:
            self.model.cuda()



    def _log_parameters_list(self) -> None:
        model_parameters = filter(lambda p: p.requires_grad,
                                  self.model.parameters())
        n_params = sum([np.prod(p.size()) for p in model_parameters])
        self.logger.info("Total params: %d", n_params)
        trainable_params = [n for (n, p) in self.model.named_parameters()
                            if p.requires_grad]
        self.logger.info("Trainable prameters: %s", sorted(trainable_params))
        assert trainable_params

    def train_and_validate(self, train_data: Dataset, valid_data: Dataset) -> None:
        train_loader = make_data_loader(train_data,
                                        batch_size=self.batch_size,
                                        shuffle=self.shuffle)

        # for epoch_no in range(self.epochs):
        while self.epoch < self.max_epoch:
            self.logger.info("EPOCH %d", self.epoch)

            if self.scheduler is not None and self.scheduler_step_at == "epoch":
                self.scheduler.step(epoch=self.epoch)

            self.model.train()

            start = time.time()
            total_valid_duration = 0
            count = self.batch_multiplier - 1
            epoch_loss = 0.
            batch_count = 0


            for batch in train_loader:
                self.model.train()
                real_batch_size = batch['img'].shape[0]

                my_batch = MyBatch(batch['img'], batch['openpose'], batch['mask'], use_cuda=self.use_cuda)


                update = count == 0
                batch_loss = self._train_mybatch(my_batch, update=update)  # tj : self.steps will increase

                self.tb_writer.add_scalar("train/batch_loss", batch_loss,
                                          self.steps)
                count = self.batch_multiplier if update else count
                count -= 1
                epoch_loss += batch_loss.detach().cpu().numpy()
                batch_count += 1

                if self.scheduler is not None and \
                        self.scheduler_step_at == "step" and update:
                    self.scheduler.step()

                # log learning progress
                if self.steps % self.logging_freq == 0 and update:
                    elapsed = time.time() - start - total_valid_duration
                    self.logger.info(
                        "Epoch %3d Step: %8d Batch Loss: %12.6f Lr: %12.8f",
                        self.epoch, self.steps, batch_loss, self.optimizer.param_groups[0]["lr"])
                    start = time.time()
                    total_valid_duration = 0
                # tj : validate every n steps
                if self.steps % self.validation_freq == 0 and update:
                    dev_loss, _ = validate(
                        batch_size=self.eval_batch_size,
                        max_validation_batches=self.max_validation_batches,
                        data=valid_data,
                        model=self.model,
                        use_cuda=self.use_cuda,
                        loss_function=self.loss
                    )
                    self.tb_writer.add_scalar("validate/dev_loss",
                                              dev_loss, self.epoch)


                    if self.early_stopping_metric == "loss":
                        ckpt_score = dev_loss
                    else:
                        raise ConfigurationError("Nothing else supported yet!")

                    new_best = False

                    if self.is_best(ckpt_score):
                        self.best_ckpt_score = ckpt_score
                        self.best_ckpt_iteration = self.steps
                        self.logger.info(
                            'Hooray! New best validation result [%s]!',
                            self.early_stopping_metric)
                        if self.ckpt_queue.maxsize > 0:
                            self.logger.info("Saving new checkpoint.")
                            new_best = True
                            self._save_checkpoint()

                    if self.scheduler is not None \
                            and self.scheduler_step_at == "validation":
                        self.scheduler.step(ckpt_score)

                    # tj : cal for train every n steps
                    if batch_count != 0:
                        ave_epoch_loss = epoch_loss / batch_count  # tj : ave batch loss in this epoch

                    self.tb_writer.add_scalar("train/epoch_loss",
                                              ave_epoch_loss, self.epoch)



                        # append to validation report
                    self._add_report(
                        train_loss=ave_epoch_loss,
                        dev_loss=dev_loss,
                        new_best=new_best)
                    self.logger.info(
                        'Validation result at epoch %3d, '
                        'step %8d: loss: %8.6f '
                        , self.epoch, self.steps,
                        dev_loss)
                if self.stop:
                    break
            if self.stop:
                self.logger.info(
                    'Training ended since minimum lr %12.8f was reached.',
                    self.learning_rate_min
                )
                self.logger.info('Training ended after %3d epochs.', self.epoch)
                break

            self.epoch += 1

        self.tb_writer.close()  # close Tensorboard writer

    def _train_mybatch(self, batch: MyBatch, update: bool = True) -> (Tensor, Tensor, Tensor):

        keypoint_output, _ = self.model.forward(
            img=batch.img
        )

        # compute batch loss
        # assert self.step == 1, "step2 needs cls"
        batch_loss = self.loss(keypoint_output, batch.openpose, batch.mask)

        if update:
            self.optimizer.zero_grad()

        batch_loss.backward()

        if self.clip_grad_fun is not None:
            # clip gradients (in-place)
            self.clip_grad_fun(params=self.model.parameters())

        if update:
            self.optimizer.step()
            # increment step counter
            self.steps += 1

        return batch_loss



    def _train_batchwithmaskcls(self, batch: BatchWithMaskCls, update: bool = True) -> (Tensor, Tensor, Tensor):

        batch_loss,_,_ = self.model.get_loss_for_batchwithmaskcls(
            batch=batch, loss_function=self.loss)


        # division needed since loss.backward sums the gradients until updated
        norm_batch_multiply = batch_loss / self.batch_multiplier

        norm_batch_multiply.backward()

        if self.clip_grad_fun is not None:
            # clip gradients (in-place)
            self.clip_grad_fun(params=self.model.parameters())

        if update:
            self.optimizer.step()
            self.optimizer.zero_grad()

            # increment step counter
            self.steps += 1

        return batch_loss



    def _add_report(self, train_loss: float, dev_loss:float, new_best: bool = False) -> None:
        """
        Append a one-line report to validation logging file.

        :param valid_score: validation evaluation score [eval_metric]
        :param valid_loss: validation loss (sum over whole validation set)
        :param eval_metric: evaluation metric, e.g. "bleu"
        :param new_best: whether this is a new best model
        """
        current_lr = -1
        # ignores other param groups for now
        for param_group in self.optimizer.param_groups:
            current_lr = param_group['lr']

        if current_lr < self.learning_rate_min:
            self.stop = True

        with open(self.valid_report_file, 'a') as opened_file:
            opened_file.write(
                "Epoch: {}\tSteps: {}\ttrain Loss: {:.5f}\tdev loss: {:.5f}\tLR: {:.8f}\t{}\n".format(
                    self.epoch, self.steps, train_loss, dev_loss, current_lr, "*" if new_best else ""))



def train(cfg_file: str) -> None:

    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    #training_data, dev_data, test_data = load_data(data_cfg=cfg["data"])


    training_data = load_train_data(data_cfg=cfg["data"])
    val_data = load_val_data(data_cfg=cfg["data"])

    model = build_model(cfg["model"], num_classes=274)

    trainer = TrainManager(model=model, config=cfg)  # tj : all the setting pass to its member

    trainer.logger.info("train frames:{}".format(training_data.__len__()))
    trainer.logger.info("val frames:{}".format(val_data.__len__()))


    shutil.copy2(cfg_file, trainer.model_dir + "/config.yaml")

    # log all entries of config
    log_cfg(cfg, trainer.logger)

    #TODO : log data info

    trainer.logger.info(str(model))

    trainer.train_and_validate(train_data=training_data, valid_data=val_data)

