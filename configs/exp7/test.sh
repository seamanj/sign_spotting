#!/bin/bash

cd /vol/research/SignPose/tj/Workspace/Skeletor_New/exp7
/vol/research/SignPose/tj/Software/miniconda3/envs/BERT_skeleton/bin/python -m src --mode=test --config_path=./configs/exp7/config.yaml --ckpt=./models/exp7/best.ckpt
